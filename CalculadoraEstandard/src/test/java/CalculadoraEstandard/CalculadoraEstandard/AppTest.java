package CalculadoraEstandard.CalculadoraEstandard;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import metodos.metodos;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }



    
    public void testSuma() {
    	CalculadoraEstandardFrame calculadora = new CalculadoraEstandardFrame();
        double esperado = 4;
        double delta = 1;
        CalculadoraEstandardFrame.resultadoFinal = 0;
        assertEquals(metodos.resultadoSuma(2.0, 2.0,CalculadoraEstandardFrame.resultadoFinal),esperado, delta);
    }
    public void testResta() {
    	CalculadoraEstandardFrame calculadora = new CalculadoraEstandardFrame();
        double esperado = 0;
        double delta = 1;
        CalculadoraEstandardFrame.resultadoFinal = 0;
        assertEquals(metodos.resultadoResta(2.0, 2.0,CalculadoraEstandardFrame.resultadoFinal),esperado, delta);
    }
    public void testMultiplicar() {
    	CalculadoraEstandardFrame calculadora = new CalculadoraEstandardFrame();
        double esperado = 4;
        double delta = 1;
        CalculadoraEstandardFrame.resultadoFinal = 0;
        assertEquals(metodos.resultadoMultiplicar(2.0, 2.0,CalculadoraEstandardFrame.resultadoFinal),esperado, delta);
    }
    public void testDividir() {
    	CalculadoraEstandardFrame calculadora = new CalculadoraEstandardFrame();
        double esperado = 1;
        double delta = 1;
        CalculadoraEstandardFrame.resultadoFinal = 0;
        assertEquals(metodos.resultadoDividir(2.0, 2.0,CalculadoraEstandardFrame.resultadoFinal),esperado, delta);
    }
    public void testPorcentaje() {
    	CalculadoraEstandardFrame calculadora = new CalculadoraEstandardFrame();
        double esperado = 1;
        double delta = 1;
        CalculadoraEstandardFrame.resultadoFinal = 0;
        assertEquals(metodos.resultadoPorcentaje(2.0, 2.0,CalculadoraEstandardFrame.resultadoFinal),esperado, delta);
    }

}
