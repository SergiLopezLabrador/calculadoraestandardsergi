package metodos;

import javax.swing.JOptionPane;


import CalculadoraEstandard.CalculadoraEstandard.CalculadoraEstandardFrame;

public class metodos {
	
	public static void division() {

		CalculadoraEstandardFrame.referenciaOperacion = "/";
		CalculadoraEstandardFrame.numero2 =  Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText());
		CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(CalculadoraEstandardFrame.textFieldPrincipal.getText() + "/");
		CalculadoraEstandardFrame.textFieldPrincipal.setText(" ");
	}
	public static void porcentaje() {
		
		CalculadoraEstandardFrame.referenciaOperacion = "%";
		CalculadoraEstandardFrame.numero2 =  Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText());
		CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(CalculadoraEstandardFrame.textFieldPrincipal.getText() + "%");
		CalculadoraEstandardFrame.textFieldPrincipal.setText(" ");
	}
	
	public static void multiplicar() {
		CalculadoraEstandardFrame.referenciaOperacion = "*";
		CalculadoraEstandardFrame.numero2 =  Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText());
		CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(CalculadoraEstandardFrame.textFieldPrincipal.getText() + "*");
		CalculadoraEstandardFrame.textFieldPrincipal.setText(" ");
	}
	public static void restar() {
		CalculadoraEstandardFrame.referenciaOperacion = "-";
		CalculadoraEstandardFrame.numero2 =  Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText());
		CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(CalculadoraEstandardFrame.textFieldPrincipal.getText() + "-");
		CalculadoraEstandardFrame.textFieldPrincipal.setText(" ");
	}
	public static void sumar() {
		CalculadoraEstandardFrame.referenciaOperacion = "+";
		CalculadoraEstandardFrame.numero2 =  Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText());
		CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(CalculadoraEstandardFrame.textFieldPrincipal.getText() + "+");
		CalculadoraEstandardFrame.textFieldPrincipal.setText(" ");
	}
	public static void resultado() {
		CalculadoraEstandardFrame.numero1 = Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText());
		
		if (CalculadoraEstandardFrame.referenciaOperacion.contentEquals("+")) {
			resultadoSuma(CalculadoraEstandardFrame.numero1, CalculadoraEstandardFrame.numero2,CalculadoraEstandardFrame.resultadoFinal);
		}
		if (CalculadoraEstandardFrame.referenciaOperacion.contentEquals("-")) {
			resultadoResta(CalculadoraEstandardFrame.numero1, CalculadoraEstandardFrame.numero2,CalculadoraEstandardFrame.resultadoFinal);
		}
		if (CalculadoraEstandardFrame.referenciaOperacion.contentEquals("*")) {
			resultadoMultiplicar(CalculadoraEstandardFrame.numero1, CalculadoraEstandardFrame.numero2,CalculadoraEstandardFrame.resultadoFinal);
		}
		if (CalculadoraEstandardFrame.referenciaOperacion.contentEquals("/")) {
			resultadoDividir(CalculadoraEstandardFrame.numero1, CalculadoraEstandardFrame.numero2,CalculadoraEstandardFrame.resultadoFinal);
		}
		if (CalculadoraEstandardFrame.referenciaOperacion.contentEquals("%")) {
			resultadoPorcentaje(CalculadoraEstandardFrame.numero1, CalculadoraEstandardFrame.numero2,CalculadoraEstandardFrame.resultadoFinal);
		}
	}
	public static void sqrt() {
		CalculadoraEstandardFrame.numero2 =  Math.sqrt(Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText()));
		CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(String.valueOf(CalculadoraEstandardFrame.numero2));
		CalculadoraEstandardFrame.textFieldPrincipal.setText(String.valueOf(CalculadoraEstandardFrame.numero2));
	}
	public static void elevarADos() {
		CalculadoraEstandardFrame.referenciaOperacion = "ElevarADos";
		CalculadoraEstandardFrame.numero2 =  Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText())*Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText());
		CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(String.valueOf(CalculadoraEstandardFrame.numero2));
		CalculadoraEstandardFrame.textFieldPrincipal.setText(String.valueOf(CalculadoraEstandardFrame.numero2));
	}
	public static void unoX() {
		CalculadoraEstandardFrame.numero2 =  1 / Double.parseDouble(CalculadoraEstandardFrame.textFieldPrincipal.getText());
		CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(String.valueOf(CalculadoraEstandardFrame.numero2));
		CalculadoraEstandardFrame.textFieldPrincipal.setText(String.valueOf(CalculadoraEstandardFrame.numero2));
	}
	public static double resultadoSuma(double numero1, double numero2 ,double resultadoFinal) {
		try {
			resultadoFinal = numero1 + numero2;
			CalculadoraEstandardFrame.textFieldPrincipal.setText(String.valueOf(resultadoFinal));
			CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(" ");
			CalculadoraEstandardFrame.textAreaHistorial.setText(CalculadoraEstandardFrame.textAreaHistorial.getText() + (numero2 + "+" + numero1 + "=" + resultadoFinal + "\n"));
			
		} catch (Exception e) {
			JOptionPane.showInternalMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultadoFinal;
	}
	public static double resultadoResta(double numero1, double numero2 ,double resultadoFinal) {
		try {
			resultadoFinal = numero1 - numero2;
			CalculadoraEstandardFrame.textFieldPrincipal.setText(String.valueOf(resultadoFinal));
			CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(" ");
			CalculadoraEstandardFrame.textAreaHistorial.setText(CalculadoraEstandardFrame.textAreaHistorial.getText() + (numero2 + "-" + numero1 + "=" + resultadoFinal + "\n"));
		} catch (Exception e) {
			JOptionPane.showInternalMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultadoFinal;
	}
	public static double resultadoMultiplicar(double numero1, double numero2 ,double resultadoFinal) {
		try {
			resultadoFinal = numero1 * numero2;
			CalculadoraEstandardFrame.textFieldPrincipal.setText(String.valueOf(resultadoFinal));
			CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(" ");
			CalculadoraEstandardFrame.textAreaHistorial.setText(CalculadoraEstandardFrame.textAreaHistorial.getText() + (numero2 + "*" + numero1 + "=" + resultadoFinal + "\n"));
		} catch (Exception e) {
			JOptionPane.showInternalMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultadoFinal;
	}
	public static double resultadoDividir(double numero1, double numero2 ,double resultadoFinal) {
		try {
			resultadoFinal = numero1 / numero2;
			CalculadoraEstandardFrame.textFieldPrincipal.setText(String.valueOf(resultadoFinal));
			CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(" ");
			CalculadoraEstandardFrame.textAreaHistorial.setText(CalculadoraEstandardFrame.textAreaHistorial.getText() + (numero2 + "/" + numero1 + "=" + resultadoFinal + "\n"));
		} catch (Exception e) {
			JOptionPane.showInternalMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultadoFinal;
	}
	public static double resultadoPorcentaje(double numero1, double numero2 ,double resultadoFinal) {
		try {
			resultadoFinal = (numero1/100) * numero2;
			CalculadoraEstandardFrame.textFieldPrincipal.setText(String.valueOf(resultadoFinal));
			CalculadoraEstandardFrame.textFieldResultadoProgresivo.setText(" ");
			CalculadoraEstandardFrame.textAreaHistorial.setText(CalculadoraEstandardFrame.textAreaHistorial.getText() + (numero2 + "%" + numero1 + "=" + resultadoFinal + "\n"));
		} catch (Exception e) {
			JOptionPane.showInternalMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultadoFinal;
	}
	
	


}
