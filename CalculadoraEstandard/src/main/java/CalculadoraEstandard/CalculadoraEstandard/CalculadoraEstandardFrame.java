package CalculadoraEstandard.CalculadoraEstandard;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import metodos.metodos;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;

public class CalculadoraEstandardFrame extends JFrame {
	
	//inicializaremos las variables aquí y les daremos el valor de public para poder utilizarlas en distinas clases

	private JPanel contentPane;
	public static JTextField textFieldPrincipal;
	public static String referenciaOperacion;
	public static double resultadoFinal;
	public static JTextField textFieldResultadoProgresivo;
	public static double numero1;
	public static double numero2;
	public static JTextArea textAreaHistorial;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculadoraEstandardFrame frame = new CalculadoraEstandardFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CalculadoraEstandardFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 709, 568);
		setTitle("Calculadora");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEstandard = new JLabel("Estandard");
		lblEstandard.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblEstandard.setBounds(10, 11, 82, 19);
		contentPane.add(lblEstandard);
		//JTextField donde mostrará el número que estás introduciendo
		textFieldPrincipal = new JTextField();
		textFieldPrincipal.setEditable(false);
		textFieldPrincipal.setBounds(10, 76, 384, 53);
		contentPane.add(textFieldPrincipal);
		textFieldPrincipal.setColumns(10);
		//Historial donde se mostrará las operaciones que has calculado
		JLabel lblHistorial = new JLabel("Historial");
		lblHistorial.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblHistorial.setBounds(517, 11, 82, 19);
		contentPane.add(lblHistorial);
		//Con este botón indicamos que queremos hacer un porcentaje
		JButton btnPorcentaje = new JButton("%");
		btnPorcentaje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				metodos.porcentaje();
			}
		});
		btnPorcentaje.setBounds(10, 140, 89, 45);
		contentPane.add(btnPorcentaje);
		//Con este botón eliminamos el contenido del JTextField inferior de la calculadora
		JButton btnCE = new JButton("CE");
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText("");
			}
		});
		btnCE.setBounds(108, 140, 89, 45);
		contentPane.add(btnCE);
		
		//Con este botón eliminamos el contenido de los dos textFields de la calculadora
		JButton btnC = new JButton("C");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText("");
				textFieldResultadoProgresivo.setText("");
			}
		});
		btnC.setBounds(207, 140, 89, 45);
		contentPane.add(btnC);
		//Aquí eliminamos el primer caracter empezando por la derecha
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(""+textFieldPrincipal.getText().substring(0, textFieldPrincipal.getText().length() - 1));
			}
		});
		btnDelete.setBounds(306, 140, 89, 45);
		contentPane.add(btnDelete);
		//Con este botón indicamos que queremos dividir
		JButton btnDividir = new JButton("/");
		btnDividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				metodos.division();
			}
		});
		btnDividir.setBounds(306, 196, 89, 45);
		contentPane.add(btnDividir);
		//Con este botón indicamos que queremos multiplicar
		JButton btnMultiplicar = new JButton("X");
		btnMultiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				metodos.multiplicar();
				
			}
		});
		btnMultiplicar.setBounds(306, 252, 89, 45);
		contentPane.add(btnMultiplicar);
		//Con este botón indicamos que queremos restar
		JButton btnRestar = new JButton("-");
		btnRestar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				metodos.restar();
			}
		});
		btnRestar.setBounds(306, 308, 89, 45);
		contentPane.add(btnRestar);
		//Con este botón indicamos que queremos sumar
		JButton btnSumar = new JButton("+");
		btnSumar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				metodos.sumar();
			}
		});
		btnSumar.setBounds(306, 364, 89, 45);
		contentPane.add(btnSumar);
		//Con este botón indicamos los resultados de las operaciones hechas a través de unos métodos creados en la clase metodos
		JButton btnIgual = new JButton("=");
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				metodos.resultado();
			}			
		});
		btnIgual.setBounds(306, 420, 89, 45);
		contentPane.add(btnIgual);
		//Con este botón indicamos que queremos hacer la raíz cuadrada
		JButton btnRaizCuadrada = new JButton("sqrt");
		btnRaizCuadrada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					metodos.sqrt();
			}
		});
		btnRaizCuadrada.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnRaizCuadrada.setBounds(207, 196, 89, 45);
		contentPane.add(btnRaizCuadrada);
		//Con este botón indicamos que queremos elevar a dos el número
		JButton btnElevarADos = new JButton("ElevarADos");
		btnElevarADos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				metodos.elevarADos();
			}
		});
		btnElevarADos.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnElevarADos.setBounds(108, 196, 89, 45);
		contentPane.add(btnElevarADos);
		
		JButton btn1x = new JButton("1/x");
		btn1x.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				metodos.unoX();
			}
		});
		btn1x.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btn1x.setBounds(10, 196, 89, 45);
		contentPane.add(btn1x);
		//Botón número 7
		JButton btn7 = new JButton("7");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"7");
			}
		});
		btn7.setBounds(10, 252, 89, 45);
		contentPane.add(btn7);
		//Botón número 8
		JButton btn8 = new JButton("8");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"8");
			}
		});
		btn8.setBounds(108, 252, 89, 45);
		contentPane.add(btn8);
		//Botón número 9
		JButton btn9 = new JButton("9");
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"9");
			}
		});
		btn9.setBounds(207, 252, 89, 45);
		contentPane.add(btn9);
		//Botón número 4
		JButton btn4 = new JButton("4");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"4");
			}
		});
		btn4.setBounds(10, 308, 89, 45);
		contentPane.add(btn4);
		//Botón número 5
		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"5");
			}
		});
		btn5.setBounds(108, 308, 89, 45);
		contentPane.add(btn5);
		//Botón número 6
		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"6");
			}
		});
		btn6.setBounds(207, 308, 89, 45);
		contentPane.add(btn6);
		//Botón número 1
		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"1");
			}
		});
		btn1.setBounds(10, 364, 89, 45);
		contentPane.add(btn1);
		//Botón número 2
		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"2");
			}
		});
		btn2.setBounds(108, 364, 89, 45);
		contentPane.add(btn2);
		//Botón número 3
		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"3");
			}
		});
		btn3.setBounds(207, 364, 89, 45);
		contentPane.add(btn3);
		//Botón indicando que usaremos decimales
		JButton btnComa = new JButton(".");
		btnComa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText() + ".");
			}
		});
		btnComa.setBounds(207, 420, 89, 45);
		contentPane.add(btnComa);
		//Botón número 0
		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldPrincipal.setText(textFieldPrincipal.getText()+"0");
			}
		});
		btn0.setBounds(10, 420, 187, 45);
		contentPane.add(btn0);
		
		textFieldResultadoProgresivo = new JTextField();
		textFieldResultadoProgresivo.setEditable(false);
		textFieldResultadoProgresivo.setBounds(10, 45, 384, 20);
		contentPane.add(textFieldResultadoProgresivo);
		textFieldResultadoProgresivo.setColumns(10);
		
		textAreaHistorial = new JTextArea();
		textAreaHistorial.setEditable(false);
		textAreaHistorial.setBounds(457, 43, 175, 420);
		contentPane.add(textAreaHistorial);
	}
}
